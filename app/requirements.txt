Django==3.2
pandas
postgres
psycopg2-binary
psycopg2
psycopg2-pool
geocoder
djangorestframework
pytz
graphene-django
django-filter
django-graphql-jwt