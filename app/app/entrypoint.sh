#!/bin/bash
python ./manage.py makemigrations metrobus &&
python ./manage.py migrate &&
python ./metrobus/upload_data.py &&
python ./manage.py runserver 0.0.0.0:8000