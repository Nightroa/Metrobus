import graphene
from graphene_django import DjangoObjectType
from .models import *


class Get_MetrobusType(DjangoObjectType):
    class Meta:
        model = Get_Metrobus
        
class AlcaldiaType(DjangoObjectType):
    class Meta:
        model = Alcaldia
        
class Query(graphene.ObjectType):
    metrobus = graphene.List(Get_MetrobusType)
    alcaldias = graphene.List(AlcaldiaType)
    
    def resolve_metrobus(self, info, **kwargs):
        return Get_Metrobus.objects.all()
        
    def resolve_alcaldias(self, info, **kwargs):
        return Alcaldia.objects.all()