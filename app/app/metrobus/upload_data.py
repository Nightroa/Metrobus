import psycopg2
import pandas as pd
import geocoder
import os

def conect():
    """Esta funcion se encarga de realizar la conexion con la base de datos"""
    try:
        conn = psycopg2.connect(
            database = os.getenv('DATABASE_NAME'),
            user = os.getenv('DATABASE_USER'),
            password = os.getenv('DATABASE_PASSWORD'),
            host = os.getenv('DATABASE_HOST'))
        
        print("conexion exitosa")
        cursor = conn.cursor()
        return cursor, conn
    except Exception as ex:
        print(ex)
    
def upload_data():
    """Esta funcion se encarga de procesar los datos para posteriormente insertarlos y convertir las coordenadas en sus ubicaciones"""
    psql, conn = conect()
    df_alcaldias = pd.read_csv('./metrobus/alcaldias.csv', usecols=['id','nomgeo'])
    query = """ SELECT * FROM alcaldia """
    table_alcaldias = pd.io.sql.read_sql(query,conn)
    df_alcaldias = drop_rows(df_alcaldias, table_alcaldias, 'alcaldia')
    insert(psql, conn, df_alcaldias, 'alcaldia')
    
    
    df_metrobus = pd.read_csv("./metrobus/prueba_fetchdata_metrobus.csv")
    alcaldia = []
    ubicacion = []
    for i in df_metrobus['geographic_point']:
        c = i.split(',')
        g = geocoder.arcgis([c[0], c[1]], method='reverse')
        ubicacion.append(g.address)
        query = """SELECT id from alcaldia where alcaldia_name like '%{}'""".format(g.city)
        psql.execute(query)
        row = psql.fetchone()
        if row != None:
            alcaldia.append(row[0])
        else:
            alcaldia.append(None)
        
    df_metrobus['ubicacion'] = ubicacion
    df_metrobus['alcaldia'] = alcaldia
    query = """ SELECT * FROM metrobus """
    table_metrobus = pd.io.sql.read_sql(query,conn)
    df_metrobus = drop_rows(df_metrobus, table_metrobus, 'metrobus')
    
    insert(psql, conn, df_metrobus, 'metrobus')
    

def insert(psql, conn, df, bandera= None):
    """Función que se encarga de insertar los datos en la base de datos"""
    if not df.empty:
        for n in df.itertuples():
            if 'metrobus' in bandera:
                query = """ INSERT INTO metrobus (date_updated,vehicle_id,vehicle_label,vehicle_current_status,position_latitude,position_longitude,geographic_point,position_speed,position_odometer,trip_schedule_relationship,trip_id,trip_start_date,trip_route_id, ubicacion, alcaldia_id) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
                data = n.date_updated, n.vehicle_id, n.vehicle_label, n.vehicle_current_status, n.position_latitude, n.position_longitude, n.geographic_point, n.position_speed, n.position_odometer, n.trip_schedule_relationship, n.trip_id, n.trip_start_date, n.trip_route_id, n.ubicacion, n.alcaldia
                psql.execute(query, data)
                conn.commit()
            elif 'alcaldia' in bandera:
                query = """ INSERT INTO alcaldia (alcaldia_name) VALUES ('{}')""".format(n.nomgeo)
                psql.execute(query)
                conn.commit()
    else:
        pass
    
def drop_rows(df, df_sql, bandera=None):
    """Se encarga de dropear las filas que ya existen en la base de datos"""
    if 'alcaldia' in bandera:
        df_sql.drop(columns=['id'], inplace=True)
        df_sql.rename(columns={'alcaldia_name':'nomgeo'}, inplace=True)
    elif 'metrobus' in bandera:
        df_sql.drop(columns=['id'], inplace=True)
        df_sql.rename(columns={'alcaldia_id':'alcaldia'}, inplace=True)
    df.drop(columns='id', inplace=True)
    df = df.merge(df_sql, how='outer', indicator='union')
    df.reset_index(inplace=True)
    df = df[df.union == 'left_only']
    if len(df)>0:
        df.drop(columns='union',inplace=True)
    else:
        pass
    return df
    
upload_data()