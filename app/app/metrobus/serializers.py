from rest_framework import serializers

from metrobus import models

class MetroSerializer(serializers.ModelSerializer):
    """ Serializa objeto de Metrobus nos permite obtener los datos de la tabla y mostrarlos en la vista"""
    
    class Meta:
        model = models.Get_Metrobus
        fields = ('id', 'vehicle_id', 'vehicle_label', 'vehicle_current_status', 'ubicacion', 'alcaldia_id')
        
class AlcaldiaSerializer(serializers.ModelSerializer):
    """ Serializa objeto de Alcaldia nos permite obtener los datos de la tabla y mostrarlos en la vista"""
    
    class Meta:
        model = models.Alcaldia
        fields = ('id','alcaldia_name')
        
class MetrobusAlcaldiaSerializer(serializers.ModelSerializer):
    """ Serializa objeto de Alcaldia union con Metrobus nos permite obtener los datos de la tabla y mostrarlos en la vista"""
    
    class Meta:
        model = models.Metrobus_Alcaldia
        fields = ('id', 'vehicle_id', 'vehicle_label', 'vehicle_current_status', 'ubicacion', 'alcaldia_id', 'alcaldia_name')
    