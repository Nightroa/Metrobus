from requests import request
from rest_framework.response import Response
from rest_framework import viewsets
from metrobus import serializers, models
# Create your views here.

class MetrobusViewSet(viewsets.ModelViewSet):
    """ Obtener datos del metrobus """
    queryset = models.Get_Metrobus.objects.raw('SELECT metrobus.id, metrobus.vehicle_id, metrobus.vehicle_label, metrobus.vehicle_current_status, metrobus.ubicacion, alcaldia.alcaldia_name FROM metrobus INNER JOIN alcaldia ON metrobus.alcaldia_id = alcaldia.id')
    serializer_class = serializers.MetrobusAlcaldiaSerializer
    
    def create(self, request, *args, **kwargs):
        """ Peticion post que devuelve los datos del metrobus y alcaldia """
        json_value = request.data
        if json_value.get('id'):
            id = models.Get_Metrobus.objects.raw('SELECT metrobus.id, metrobus.vehicle_id, metrobus.vehicle_label, metrobus.vehicle_current_status, metrobus.ubicacion, alcaldia.alcaldia_name FROM metrobus INNER JOIN alcaldia ON metrobus.alcaldia_id = alcaldia.id where metrobus.id = %s', [json_value.get('id')])
            serializer = serializers.MetrobusAlcaldiaSerializer(id, many=True)
        elif json_value.get('vehicle_id'):
            vehicle_id = models.Get_Metrobus.objects.raw('SELECT metrobus.id, metrobus.vehicle_id, metrobus.vehicle_label, metrobus.vehicle_current_status, metrobus.ubicacion, alcaldia.alcaldia_name FROM metrobus INNER JOIN alcaldia ON metrobus.alcaldia_id = alcaldia.id where metrobus.vehicle_id = %s', [json_value.get('vehicle_id')])
            serializer = serializers.MetrobusAlcaldiaSerializer(vehicle_id, many=True)
        elif json_value.get('vehicle_label'):
            vehicle_label = models.Get_Metrobus.objects.raw('SELECT metrobus.id, metrobus.vehicle_id, metrobus.vehicle_label, metrobus.vehicle_current_status, metrobus.ubicacion, alcaldia.alcaldia_name FROM metrobus INNER JOIN alcaldia ON metrobus.alcaldia_id = alcaldia.id where metrobus.vehicle_label = %s', [json_value.get('vehicle_label')])
            serializer = serializers.MetrobusAlcaldiaSerializer(vehicle_label, many=True)
        elif json_value.get('vehicle_current_status'):
            vehicle_current_status = models.Get_Metrobus.objects.raw('SELECT metrobus.id, metrobus.vehicle_id, metrobus.vehicle_label, metrobus.vehicle_current_status, metrobus.ubicacion, alcaldia.alcaldia_name FROM metrobus INNER JOIN alcaldia ON metrobus.alcaldia_id = alcaldia.id where metrobus.vehicle_current_status = %s', [json_value.get('vehicle_current_status')])
            serializer = serializers.MetrobusAlcaldiaSerializer(vehicle_current_status, many=True)
        elif json_value.get('ubicacion'):
            ubicacion = models.Get_Metrobus.objects.select_related().filter(ubicacion__icontains=json_value.get('ubicacion'))
            serializer = serializers.MetroSerializer(ubicacion, many=True)
        elif json_value.get('alcaldia_id'):
            alcaldia_id = models.Get_Metrobus.objects.raw('SELECT metrobus.id, metrobus.vehicle_id, metrobus.vehicle_label, metrobus.vehicle_current_status, metrobus.ubicacion, alcaldia.alcaldia_name FROM metrobus INNER JOIN alcaldia ON metrobus.alcaldia_id = alcaldia.id where metrobus.alcaldia_id = %s', [json_value.get('alcaldia_id')])
            serializer = serializers.MetrobusAlcaldiaSerializer(alcaldia_id, many=True)
        elif json_value.get('alcaldia_name'):
            alcaldia = models.Alcaldia.objects.raw("SELECT metrobus.id, metrobus.vehicle_id, metrobus.vehicle_label, metrobus.vehicle_current_status, metrobus.ubicacion, alcaldia.alcaldia_name FROM metrobus INNER JOIN alcaldia ON metrobus.alcaldia_id = alcaldia.id where alcaldia.alcaldia_name  like %s", [json_value.get('alcaldia_name')])
            serializer = serializers.MetrobusAlcaldiaSerializer(alcaldia, many=True)
        return Response(serializer.data)
    
    
class AlcaldiaViewSet(viewsets.ModelViewSet):
    """ Obtener datos del metrobus """
    queryset = models.Alcaldia.objects.all()
    serializer_class = serializers.AlcaldiaSerializer
    
    def create(self, request, *args, **kwargs):
        """ peticion post que devuelve los datos de la alcaldia """
        json_value = request.data
        if json_value.get('id'):
            id = models.Alcaldia.objects.filter(id=json_value.get('id'))
            serializer = serializers.AlcaldiaSerializer(id, many=True)
        elif json_value.get('alcaldia_name'):
            alcaldia_name = models.Alcaldia.objects.select_related().filter(alcaldia_name__icontains=json_value.get('alcaldia_name'))
            serializer = serializers.AlcaldiaSerializer(alcaldia_name, many=True)
        return Response(serializer.data)