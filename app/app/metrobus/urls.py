from django.db import router
from django.urls import path, include

from rest_framework.routers import DefaultRouter

from metrobus import views

"""Registro de las vistas de la aplicación metrobus"""
router = DefaultRouter()
router.register('metrobus', views.MetrobusViewSet)
router.register('alcaldia', views.AlcaldiaViewSet)

urlpatterns = [
    path('', include(router.urls))
]
