from django.contrib import admin
from .models import Get_Metrobus, Alcaldia

# Register your models here.
admin.site.register(Get_Metrobus)
admin.site.register(Alcaldia)
