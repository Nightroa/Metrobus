from django.db import models

# Create your models here.

class Alcaldia(models.Model):
    """Creacion del modelo de alcaldia (tabla, campos de la base de datos)"""
    alcaldia_name = models.CharField(max_length=50)
    
    
    class Meta:
        verbose_name = "alcaldia"
        verbose_name_plural = "alcaldias"
        db_table = 'alcaldia'

class Get_Metrobus(models.Model):
    """Creacion del modelo de Metrobus (tabla, campos de la base de datos)"""
    date_updated = models.CharField(max_length=30)
    vehicle_id = models.IntegerField()
    vehicle_label = models.IntegerField()

    vehicle_current_status = models.IntegerField()
    position_latitude = models.FloatField()
    position_longitude = models.FloatField()

    geographic_point = models.CharField(max_length=250)
    position_speed = models.IntegerField()
    position_odometer = models.IntegerField()

    trip_schedule_relationship = models.IntegerField()
    trip_id = models.FloatField()
    trip_start_date = models.FloatField()

    trip_route_id = models.FloatField()
    ubicacion = models.CharField(max_length=250)
    alcaldia = models.ForeignKey(Alcaldia, on_delete=models.CASCADE)
    
    class Meta:
        verbose_name = "Metrobus"
        verbose_name_plural = "Metrobus"
        db_table = 'metrobus'
        
class Metrobus_Alcaldia(models.Model):
    """ Se crea el modelo para la union de las tablas alcaldia y metrobus """
    vehicle_id =models.IntegerField()
    vehicle_label =models.IntegerField()
    vehicle_current_status =models.IntegerField()
    ubicacion =models.CharField(max_length=250)
    alcaldia_id = models.ForeignKey(Alcaldia, on_delete=models.CASCADE, null=True)
    alcaldia_name = models.CharField(max_length=50)