<!-- PROJECT -->
<br />
<p align="center">
  <h3 align="center">API Metrobus</h3>
</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
    <summary><b>Tabla de contenidos</b></summary>
    <ol>
        <li>Diagrama de funcionamiento</li>
        <li>Lógica del proceso</li>
        <li>Contenido del paquete</li>
        <li>Despliegue</li>
        <li>Resultados</li>
    </ol>
</details>


<br>
<br>

# Diagrama de funcionamiento
![desc](./IMG/d1.png)

<br>
<br>

# Lógica del proceso

Se requiere una API que pueda consultar, ya sea por ID, Alcaldia o estatus, las unidades y alcaldias disponibles del Metrobus de CDMX, consultar la ubicacion de cada unidad dando su id, y las unidades que se encuentren en una alcaldia.

La API cuenta con 3 endpoints, los cuales procesan la peticion enviada, realiza un filtrado de datos y retorna la respuesta en formato JSON.

# Enpoints

1- metrobus
2- alcaldia
3- graphql



<br>


**USO**

La data que se le suministra a la API, debe de ir colocada en formato de JSON en una representanción de cadena de texto. 

**EJEMPLOS**

**Endpoint http://localhost:8000/api/metrobus/**

```json

{
  "vehicle_id": "170"
}

```

En caso de ser exitoso, el envío, la API regresará una respuesta como esta:

```json

[
    {
        "id": 1,
        "vehicle_id": 170,
        "vehicle_label": 112,
        "vehicle_current_status": 2,
        "alcaldia": "Metrobus 1, Ciudad Universitaria, Coyoacán, Ciudad de México, 04360"
    }
]


```


**Endpoint http://localhost:8000/api/alcaldia/**


```json

{

  "alcaldia_name": "Milpa Alta"

}

```

En caso de ser exitoso, el envío, la API regresará una respuesta como esta:

```json

[
    {
        "id": 1,
        "alcaldia_name": "Milpa Alta"
    }
]


```


**Endpoint http://localhost:8000/graphql/**

```graphql

query{
  metrobus{
    id
    vehicleId
    vehicleLabel
    vehicleCurrentStatus
    ubicacion
    alcaldia{
      id
      alcaldiaName
    }
  }
}

```

En caso de ser exitoso, el envío, la API regresará una respuesta como esta:

```json

{
  "data": {
    "metrobus": [
      {
        "id": "1",
        "vehicleId": 170,
        "vehicleLabel": 112,
        "vehicleCurrentStatus": 2,
        "ubicacion": "Metrobus 1, Ciudad Universitaria, Coyoacán, Ciudad de México, 04360",
        "alcaldia": {
          "id": "4",
          "alcaldiaName": "Coyoacán"
        }
      },
      {
        "id": "2",
        "vehicleId": 177,
        "vehicleLabel": 119,
        "vehicleCurrentStatus": 1,
        "ubicacion": "Metrobus 1, Tlalpan Centro, Tlalpan, Ciudad de México, 14000",
        "alcaldia": {
          "id": "13",
          "alcaldiaName": "Tlalpan"
        }
      },
      {
        "id": "3",
        "vehicleId": 1286,
        "vehicleLabel": 219,
        "vehicleCurrentStatus": 2,
        "ubicacion": "Metrobus Canal del Norte",
        "alcaldia": {
          "id": "12",
          "alcaldiaName": "Venustiano Carranza"
        }
      },
    ]
  }
}

```




**Nota los endpoints pueden cambiar al momento de usar kubernetes ejemplo (http://localhost:62236/api/metrobus/, http://localhost:62236/api/alcaldia/ y http://localhost:62236/graphql/)**

# Contenido del paquete

En la siguiente imagen se muestra el contendio del paquete. Con los archivos **Dockerfile y requirements.txt** se puede crear la imagen de docker. Con los archivos **0-secrets.yaml y 1-kubernetes.yaml** se puede desplegar el proceso en AKS. Contamos con los **secrests y kubernets** del contenedor de postgresql (DB) y la API de Django (metrobus), esto ya que es necesario tener 2 pods corriento y su comunicacion para el correcto funcionamiento de este proceso.

# Despliegue del Proceso

**Dockerfile** 

``` dockerfile
FROM python:3.9
ENV PYTHONUNBUFFERED 1

# Install python libraries
COPY requirements.txt .
RUN pip install --upgrade pip && \
    pip install -r requirements.txt && \
    rm  requirements.txt

# Copy the source code
RUN mkdir /app
COPY ./app /app
WORKDIR /app
```

Se proporciona el **Dockerfile** para crear la imagen de Docker:

```shell
sudo docker build -t image-name:tag .

```

**docker-compose**

``` yaml

version: '3.1'

services:

  app:
    build: 
      context: .
    ports:
      - "8000:8000"
    volumes:
      - ./app:/app
    command: >
      sh -c "./entrypoint.sh"
    depends_on:
      - postgres
    networks:
      - postgres-server
    environment:
      - DATABASE_NAME=postgres
      - DATABASE_PASSWORD=toor
      - DATABASE_USER=root
      - DATABASE_HOST=postgres

  postgres:
    image: postgres
    restart: always
    ports:
      - 5432:5432
    environment:
      - POSTGRES_USER=root
      - POSTGRES_PASSWORD=toor
      - POSTGRES_DB=postgres
    networks:
      - postgres-server

networks:
  postgres-server:
    driver: bridge



```

<br>

En el archivo **0-secrets.yaml** se tienen que agregar las variables de ambiente que requiere el proceso:

<br>

**POSTGRESQL**

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: postgres-secrets
  namespace: metrobus-api
type: Opaque
stringData:
  user : root
  password : toor
  DATABASE_NAME : postgres
```

**user**: Usuario de la base de datos.

**password**: Contraseña del usuario de base de datos.

**DATABASE_NAME**: Nombre de la base de datos.

<br>

**DJANGO**

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: metrobus-secrets
  namespace: metrobus-api
type: Opaque
stringData:
  DATABASE_NAME : postgres
  DATABASE_USER : root
  DATABASE_PASSWORD : toor
  DATABASE_HOST : postgres

```



**DATABASE_NAME**: Nombre de la base de datos.

**DATABASE_USER**: Usuario de la base de datos.

**DATABASE_PASSWORD**: Contraseña del usuario de base de datos.

**DATABASE_HOST** : Nombre indicado en el service del archivo kubernetes de postgresql


En el archivo **1-kubernetes.yaml** se tiene que especificar la imagen de docker que tiene que descargar, los recursos que se le van a asignar, el namespace.

<br>

**POSTGRES**

<br>

```yaml

apiVersion: v1
kind: Namespace
metadata:
  name: metrobus-api

---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: postgresql-pv
  namespace: metrobus-api
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi

---
apiVersion: apps/v1
kind: Deployment
metadata:
 name: postgres
 namespace: metrobus-api
spec:
 strategy:
   rollingUpdate:
     maxSurge: 1
     maxUnavailable: 1
   type: RollingUpdate
 replicas: 1
 selector:
   matchLabels:
     app: postgres
 template:
   metadata:
     labels:
       app: postgres
   spec:
     containers:
       - name: postgres
         image: postgres
         resources:
           limits:
             cpu: "1"
             memory: "4Gi"
           requests:
             cpu: "1"
             memory: "4Gi"
         ports:
           - containerPort: 5432
         env:
          - name: POSTGRES_USER
            valueFrom:
              secretKeyRef:
                name: postgres-secrets
                key: user
          - name: POSTGRES_PASSWORD
            valueFrom:
              secretKeyRef:
                name: postgres-secrets
                key: password
          - name: DATABASE_NAME
            valueFrom:
              secretKeyRef:
                name: postgres-secrets
                key: DATABASE_NAME
     volumes:
       - name: postgredb
         persistentVolumeClaim:
           claimName: postgresql-pv

---
apiVersion: v1
kind: Service
metadata:
  name: postgres
  namespace: metrobus-api
spec:
  type: ClusterIP
  selector:
    app: postgres
  ports:
    - protocol: TCP
      port: 5432
      targetPort: 5432

---

```

<br>


**DJANGO**

<br>

```yaml

apiVersion: apps/v1
kind: Deployment
metadata:
  name: metrobus
  namespace: metrobus-api
  labels:
    app: metrobus
spec:
  replicas: 1
  selector:
    matchLabels:
      app: metrobus
  template:
    metadata:
      labels: 
        app: metrobus
    spec:
      containers:
        - name: metrobus
          image: nightroa/metrobus:v0.4
          command:
          - /bin/sh
          - -c
          - |
            ./entrypoint.sh
          ports:
            - containerPort: 8000
          resources:
            requests: 
              cpu: 250m
              memory: 300Mi
            limits:
              cpu: 350m
              memory: 400Mi
          env:
          - name: DATABASE_HOST
            valueFrom:
              secretKeyRef:
                name: metrobus-secrets
                key: DATABASE_HOST
          - name: DATABASE_USER
            valueFrom:
              secretKeyRef:
                name: metrobus-secrets
                key: DATABASE_USER
          - name: DATABASE_PASSWORD
            valueFrom:
              secretKeyRef:
                name: metrobus-secrets
                key: DATABASE_PASSWORD
          - name: DATABASE_NAME
            valueFrom:
              secretKeyRef:
                name: metrobus-secrets
                key: DATABASE_NAME

---
apiVersion: v1
kind: Service
metadata:
  namespace: metrobus-api
  name: metrobus-app
spec:
  type: LoadBalancer
  selector:
    app: metrobus
  ports:
  - protocol: TCP
    port: 8000
    targetPort: 8000


```

Una ves realizado esto, se puede montar el pod con:

**NOTA: Primero crear el namespace metrobus-api despues montar el secret y manifiesto (kubernetes) de postgres**

```shell
kubectl apply -f 0-secrets-postgres-api
kubectl apply -f 1-kubernetes-postgres-api


kubectl apply -f 0-secrets-metrobus-api
kubectl apply -f 1-kubernetes-metrobus-api

```

<br>

<br>

# Resultados

Los resultados del proceso se muestran a continuacion

<br>

**GET**

<br>

![desc](./IMG/d5.png)

<br>

![desc](./IMG/d6.png)

<br>

![desc](./IMG/d7.png)

<br>

**POST**

<br>

![desc](./IMG/d2.png)

<br>

![desc](./IMG/d3.png)

<br>

![desc](./IMG/d4.png)

<br>

![desc](./IMG/d8.png)

<br>

![desc](./IMG/d9.png)

<br>

![desc](./IMG/d10.png)

<br>

**GRAPHQL**
<br>

![desc](./IMG/d11.png)

<br>

![desc](./IMG/d12.png)

<br>

# Responsable
- [Adán Alberto Torres Crespo] - adaan_torres@hotmail.com